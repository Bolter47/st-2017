// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelHUD.h"
#include "UObject/ConstructorHelpers.h"

#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"


ALevelHUD::ALevelHUD()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> lvlWidgetClassFinder(TEXT("/Game/MenuWidgets/ScoreUI"));
	if (lvlWidgetClassFinder.Succeeded())
	    lvlWidgetClass = lvlWidgetClassFinder.Class;
}

// Called when the game starts or when spawned
void ALevelHUD::BeginPlay()
{
	Super::BeginPlay();

	if (lvlWidgetClass)
	{
		lvlWidget = CreateWidget<UUserWidget>(GetOwningPlayerController(), lvlWidgetClass);
		lvlWidget->AddToViewport();
	}

}

