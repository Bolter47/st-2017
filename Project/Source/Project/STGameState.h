// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "TeamState.h"
#include "STGameState.generated.h"

class UTeamInfo;

/**
 * 
 */
UCLASS()
class PROJECT_API ASTGameState : public AGameState
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, category = "STGameState")
	TMap<int32, UTeamState*>& GetTeams();

	UFUNCTION(BlueprintCallable, category="STGameState")
	int GetTeamScore(int32 teamId);
	UFUNCTION(BlueprintCallable, category = "STGameState")
	FColor GetTeamColor(int32 teamId);
	UFUNCTION(BlueprintCallable, category = "STGameState")
	int GetNumberOfTeam();

	UFUNCTION(BlueprintCallable, category = "STGameState")
	UTeamState* GetTeamState(int32 teamId);

	// Gets the team state associated to the player
	UTeamState* GetPlayerTeam(AController* player);

	void HandleBeginPlay() override;


	// Sets the player to be in the team they picked during pre-match.
	void AssignPlayerToTeam(int32 team, AController* player);

private:

	void SetupTeamStates();
	
	// teams mapped by Id
	UPROPERTY()
	TMap<int32, UTeamState*> Teams;
	
};
