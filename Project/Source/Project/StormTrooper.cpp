// Fill out your copyright notice in the Description page of Project Settings.

#include "StormTrooper.h"
#include "ProjectProjectile.h"
#include "ShieldComponent.h"
#include "LaserPointerComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/MaterialBillboardComponent.h"
#include "Engine/CollisionProfile.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "ConductibleComponent.h"
#include "DeathEffectsActor.h"
#include "Components/TextRenderComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Runtime/Core/Public/Internationalization/Text.h"
#include "STGameInstance.h"
#include "MeshAtlas.h"
#include "STAnimInstance.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "GameSettings.h"
#include "PlayerInfo.h"

#include <exception>

// Sets default values
AStormTrooper::AStormTrooper()
{

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
	/*** Setup the skeletal mesh ***/

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> Mesh(TEXT("SkeletalMesh'/Game/Meshes/ShockTrooper_Body_Idle_Anim.ShockTrooper_Body_Idle_Anim'"));
	// Create the mesh component
	BodyComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BodyMesh"));
	BodyComponent->bReceivesDecals = false;
	RootComponent = BodyComponent;
	BodyComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	BodyComponent->SetCollisionResponseToChannel(ECC_Vehicle, ECR_Ignore);
	BodyComponent->SetSkeletalMesh(Mesh.Object);
	
	// Animations
	static ConstructorHelpers::FObjectFinder<UClass> animClassFinder(TEXT("/Game/Meshes/StormTrooperAnimBlueprint.StormTrooperAnimBlueprint_C"));
	BodyComponent->SetAnimInstanceClass(animClassFinder.Object);

	// Get travel sound
	static ConstructorHelpers::FObjectFinder<USoundBase> TravelAudio(TEXT("SoundCue'/Game/SFX/S_FootSteps.S_FootSteps'"));
	FootstepSoundSource = CreateDefaultSubobject<UAudioComponent>(TEXT("TravelSoundComponent"));
	FootstepSoundSource->SetSound(TravelAudio.Object);
	FootstepSoundSource->bAutoActivate = false;


	// Get charge sound
	static ConstructorHelpers::FObjectFinder<USoundBase> ChargeAudio(TEXT("SoundWave'/Game/SFX/charging.charging'"));
	ChargeSoundSource = CreateDefaultSubobject<UAudioComponent>(TEXT("ChargeSoundComponent"));
	ChargeSoundSource->SetSound(ChargeAudio.Object);
	ChargeSoundSource->bAutoActivate = false;

	// Get the editable body material
	static ConstructorHelpers::FObjectFinder<UMaterial> BodyBaseMat(TEXT("Material'/Game/Materials/M_ShockTrooper-Body.M_ShockTrooper-Body'"));
	if (BodyBaseMat.Succeeded())
	{
		BodyMaterial = UMaterialInstanceDynamic::Create(BodyBaseMat.Object, this, "bodyMat");
		BodyComponent->SetMaterial(0, BodyMaterial);
	}

	/*** Setup the helmet mesh ***/
	HelmetMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HelmetMesh"));
	HelmetMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HelmetMesh->bReceivesDecals = false;
	HelmetMesh->SetupAttachment(BodyComponent, "Helmet");
	/*** Setup the gun mesh ***/
	static ConstructorHelpers::FObjectFinder<UStaticMesh> gunMeshFinder(TEXT("StaticMesh'/Game/Meshes/ShockTrooper-Gun_SM.ShockTrooper-Gun_SM'"));
	auto gunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GunMesh"));
	gunMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	gunMesh->bReceivesDecals = false;
	gunMesh->SetupAttachment(BodyComponent, "Gun");
	gunMesh->SetStaticMesh(gunMeshFinder.Object);

	ChargeBar = CreateDefaultSubobject<UMaterialBillboardComponent>(TEXT("ChargeBar"));
	ChargeBar->SetupAttachment(BodyComponent);
	ChargeBar->bAbsoluteLocation = true;
	ChargeBar->bAbsoluteRotation = true;
	ChargeBar->bHiddenInGame = false;
	ChargeBar->bReceivesDecals = false;
	ChargeBar->SetVisibility(false);

	ChargeCounter = CreateDefaultSubobject<UTextRenderComponent>(TEXT("ChargeCounter"));
	ChargeCounter->SetupAttachment(BodyComponent);
	ChargeCounter->SetWorldSize(ChargeFontSize);
	ChargeCounter->SetHorizontalAlignment(EHTA_Center);
	ChargeCounter->SetWorldRotation(FRotator(90.f, 180.f, 0.f));
	ChargeCounter->SetText(TEXT("1"));
	ChargeCounter->SetTextRenderColor(FColor::Orange);
	ChargeCounter->bReceivesDecals = false;
	ChargeCounter->SetVisibility(false);
	ChargeCounter->bAbsoluteRotation = true;
	static ConstructorHelpers::FObjectFinder<UMaterial> CounterBaseMat(TEXT("Material'/Engine/EngineMaterials/UnlitText.UnlitText'"));
	ChargeCounter->SetMaterial(0, CounterBaseMat.Object);

	static ConstructorHelpers::FObjectFinder<UMaterial> ChargeBaseMat(TEXT("/Game/Materials/M_Charge_Color"));
	if (ChargeBaseMat.Succeeded())
	{
		ChargeMaterial = UMaterialInstanceDynamic::Create(ChargeBaseMat.Object, this, "chargeMat");
		ChargeMaterial->SetScalarParameterValue("Intensity", 0.0);
		ChargeMaterial->SetScalarParameterValue("Edge", -1.0);
		ChargeBar->AddElement(ChargeMaterial, NULL, false, ChargeBarSize.X, ChargeBarSize.Y, NULL);
	}

	StunBar = CreateDefaultSubobject<UMaterialBillboardComponent>(TEXT("StunBar"));
	StunBar->SetupAttachment(BodyComponent);
	StunBar->bAbsoluteLocation = true;
	StunBar->bAbsoluteRotation = true;
	StunBar->bReceivesDecals = false;
	StunBar->bHiddenInGame = false;

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> StunBarMaterial(TEXT("/Game/Materials/M_Stun_Color"));
	if (StunBarMaterial.Object)
		StunBar->AddElement(StunBarMaterial.Object, NULL, false, StunBarSize.X, 0, NULL);

	// Cache our sound effect
	static ConstructorHelpers::FObjectFinder<USoundBase> FireAudio(TEXT("SoundCue'/Game/SFX/Shot.Shot'"));
	FireSound = FireAudio.Object;

	// Add conductibility
	auto conductible = CreateDefaultSubobject<UConductibleComponent>(TEXT("Conductor"));
	conductible->ProjectileFireOffset = GunDistance;
	conductible->ChargeReduction = ChargeKillThreshold;
	conductible->OnProjectileHit.AddDynamic(this, &AStormTrooper::TakeHit);

	// laserpointer
	static ConstructorHelpers::FObjectFinder<UStaticMesh> LaserMesh(TEXT("/Engine/BasicShapes/Cube"));
	laserPointer = CreateDefaultSubobject<ULaserPointerComponent>(TEXT("laserPointer"));
	laserPointer->SetupAttachment(BodyComponent);
	((UStaticMeshComponent*)laserPointer)->SetStaticMesh(LaserMesh.Object);
	laserPointer->SetRelativeLocation(FVector(500, 0.f, gunHeight));
	laserPointer->SetRelativeRotation(FRotator(0, -90, 90));
	laserPointer->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	laserPointer->bReceivesDecals = false;
	laserPointer->SetVisibility(false);
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> AimBarMaterial(TEXT("/Game/Materials/M_AimBar_01"));
	if (AimBarMaterial.Succeeded())
		laserPointer->SetMaterial(0, AimBarMaterial.Object);

	// Hightlight circle
	HighlightMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("highlight"));
	HighlightMesh->SetupAttachment(BodyComponent);
	HighlightMesh->SetStaticMesh(LaserMesh.Object);
	HighlightMesh->SetRelativeLocation(FVector(0.0f, 0.f, gunHeight/4));
	HighlightMesh->SetRelativeRotation(FRotator(0, 0, 0));
	HighlightMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HighlightMesh->SetRelativeScale3D(FVector(2.0f, 2.0f, 0.0f));
	HighlightMesh->bReceivesDecals = false;
	HighlightMesh->SetVisibility(false);
	// Get the editable body material
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> CircleBaseMat(TEXT("Material'/Game/Materials/M_CharacterHighlight.M_CharacterHighlight'"));
	if (CircleBaseMat.Succeeded())
	{
		HighlightMaterial = UMaterialInstanceDynamic::Create(CircleBaseMat.Object, this, "highlightMat");
		HighlightMesh->SetMaterial(0, HighlightMaterial);
	}



	// Add a spring arm to give the camera smooth, natural-feeling motion.
	USpringArmComponent* SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraAttachmentArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->RelativeRotation = FRotator(-45.f, 0.f, 0.f);
	SpringArm->TargetArmLength = 800.0f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;
	// Create a camera and attach to our spring arm for victory
	UCameraComponent* Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("VictoryCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
}

// Called when the game starts or when spawned
void AStormTrooper::BeginPlay()
{
	Super::BeginPlay();

	TargetRotation = GetActorRotation();
	laserPointer->SetRelativeScale3D(FVector(0.25, 0.f, 10));
}

void AStormTrooper::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	UpdateStun(DeltaTime);
	UpdateWeaponCharge(DeltaTime);

	// Update rotation
	if (bIsStun)
		return;

	FRotator CurrentRotation = GetActorRotation();
	float RotationStep = RotationSpeed*DeltaTime;
	if (FMath::Abs(TargetRotation.Yaw - CurrentRotation.Yaw) > RotationStep) {
		auto newRot = FMath::RInterpConstantTo(CurrentRotation, TargetRotation, DeltaTime, RotationSpeed);
		// Update the rotation
		SetActorRotation(newRot);
	}
	else {
		SetActorRotation(TargetRotation);
	}

	// update UI position
	ChargeBar->SetWorldLocation(GetActorLocation() + ChargeBarRelativeLocation);
	StunBar->SetWorldLocation(GetActorLocation() + StunBarRelativeLocation);
	ChargeCounter->SetWorldLocation(GetActorLocation() + ChargeCounterRelativeLocation);
}

void AStormTrooper::UpdateStun(float DeltaTime)
{
	if (bIsStun)
	{
		CurrentStunTime += DeltaTime;
		if (StunBar->Elements.Num() > 0)
		{
			StunBar->ToggleVisibility();
			StunBar->Elements[0].BaseSizeY = FMath::Max(MaxStunTime - CurrentStunTime, 0.f) / MaxStunTime * StunBarSize.Y;
			StunBar->ToggleVisibility();
		}

		bIsStun = !(CurrentStunTime >= MaxStunTime);
	}
}

void AStormTrooper::UpdateWeaponCharge(float DeltaTime)
{
	// Watch for float equality
	if (bIsStun && CurrentCharge > 0.001)
	{
		StopCharging();
	}



	if (bIsCharging)
	{
		ChargeSoundSource->SetPitchMultiplier(CurrentCharge/4);
		if (CurrentCharge < MaxCharge) {
			int previousCharge = FMath::FloorToInt(CurrentCharge);
			CurrentCharge = FMath::Min(CurrentCharge + ChargeRate*DeltaTime, MaxCharge);
			int nextCharge = FMath::FloorToInt(CurrentCharge);
			ChargeMaterial->SetScalarParameterValue("Edge", (CurrentCharge - nextCharge) * 2 - 1);

			if (previousCharge != nextCharge)
			{
				ChargeCounter->SetText(FText::AsNumber(nextCharge));
				ChargeMaterial->SetScalarParameterValue("Intensity", nextCharge);
				ChargeMaterial->SetScalarParameterValue("Edge", -1.0);
			}

		}
	}
}

// Called every frame
void AStormTrooper::Move(FVector MoveDirection, FRotator NewRotation, float DeltaTime)
{
	if (bIsStun)
		return;

	FVector Movement = MoveDirection*DeltaTime*MovementSpeed;

	// Apply new movement and rotation
	// Applying movement component by component to slide in case of collision
	SetActorLocation(GetActorLocation() + FVector(Movement.X, 0, 0), true);
	SetActorLocation(GetActorLocation() + FVector(0, Movement.Y, 0), true);
	SetActorLocation(GetActorLocation() + FVector(0, 0, Movement.Z), true);
	TargetRotation = NewRotation;

	// Apply to animator
	
	FRotator vectorRot = GetActorRotation() - MoveDirection.Rotation();
	((USTAnimInstance*)(BodyComponent->GetAnimInstance()))->RelativeMoveDirection = vectorRot.RotateVector(FVector(MoveDirection.Size(), 0, 0));

	// Play sound
	if (Movement.Size() > 0)
	{
		if (!FootstepSoundSource->IsPlaying()) {
			FootstepSoundSource->Play();
		}
	}
	else 
	{
		FootstepSoundSource->Stop();
	}
}

void AStormTrooper::StartCharging()
{
	bIsCharging = true;
	CurrentCharge = 1.0f;
	ChargeCounter->SetVisibility(true);
	ChargeBar->SetVisibility(true);
	ChargeSoundSource->Play();
}

void AStormTrooper::Fire()
{
	if (CurrentCharge >= 1 && !bIsStun)
	{
		const FRotator FireRotation = GetActorRotation();
		// Spawn projectile at an offset from this pawn
		const FVector SpawnLocation = GetActorLocation() + FireRotation.RotateVector(FVector(GunDistance, 0, 0)) + FVector(0, 0, gunHeight);

		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			// spawn the projectile
			AProjectProjectile* myBullet = World->SpawnActor<AProjectProjectile>(SpawnLocation, FireRotation);
			myBullet->Initialize(FMath::FloorToInt(CurrentCharge), GetController());
		}

		// try and play the sound if specified
		if (FireSound != nullptr)
		{
			UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}
		
	}
	StopCharging();
}

void AStormTrooper::StopCharging()
{
	bIsCharging = false;
	CurrentCharge = 0.0f;
	ChargeCounter->SetText("1");
	ChargeMaterial->SetScalarParameterValue("Intensity", 0.0);
	ChargeCounter->SetVisibility(false);
	ChargeBar->SetVisibility(false);
	ChargeSoundSource->Stop();
}

void AStormTrooper::TakeHit(int charges, AController* attacker)
{
	// Check if shot should be ignored due to friendly fire
	auto gameInstance = (USTGameInstance*)GetGameInstance();
	bool friendlyFireEnabled = gameInstance->GameSettings->bFriendlyFireEnabled;
	auto thisPlayerInfo = gameInstance->GetPlayerInfo(UGameplayStatics::GetPlayerControllerID(Controller->CastToPlayerController()));
	auto attackerPlayerInfo = gameInstance->GetPlayerInfo(UGameplayStatics::GetPlayerControllerID(attacker->CastToPlayerController()));
	if (!friendlyFireEnabled && attackerPlayerInfo->GetTeamId() == thisPlayerInfo->GetTeamId()) {
		return;
	}

	// if not, resolve hit
	bIsStun = true;
	CurrentStunTime = 0.0f;
	if (charges >= ChargeKillThreshold)
	{
		// DEATH
		
		// Shoot blood all over the place
		auto decal = GetWorld()->SpawnActor<ADeathEffectsActor>(GetActorLocation(), GetActorRotation());

		OnPlayerKilled.Broadcast(attacker, GetController());
	}
}

void AStormTrooper::SetVisuals(FString helmetName, FColor primaryColor, FColor secondaryColor)
{
	// Build atlas to choose from
	auto atlas = NewObject<UMeshAtlas>();

	// Get info from mesh atlas
	UStaticMesh* helmetMesh = nullptr;
	UMaterialInterface* helmetMaterial = nullptr;
	atlas->GetHelmetMesh(helmetName, helmetMesh, helmetMaterial);

	// Change helmet mesh
	HelmetMesh->SetStaticMesh(helmetMesh);
	HelmetMaterial = UMaterialInstanceDynamic::Create(helmetMaterial, this);
	HelmetMaterial->SetVectorParameterValue("PrimaryColor", FLinearColor(primaryColor));
	HelmetMaterial->SetVectorParameterValue("SecondaryColor", FLinearColor(secondaryColor));
	HelmetMesh->SetMaterial(0, HelmetMaterial);

	
	BodyMaterial->SetVectorParameterValue("PrimaryColor", FLinearColor(primaryColor));
	BodyMaterial->SetVectorParameterValue("SecondaryColor", FLinearColor(secondaryColor));

	HighlightMaterial->SetVectorParameterValue("Color", FLinearColor(primaryColor));
}

void AStormTrooper::SetLaserVisibility(bool visible)
{
	laserPointer->SetVisibility(visible);
	HighlightMesh->SetVisibility(visible);
}

