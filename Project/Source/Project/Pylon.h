// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pylon.generated.h"

UCLASS()
class PROJECT_API APylon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APylon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// Defaults (Editable in component menu in editor...)
	float PylonSize = 150.0;
	int Resistance = 1;

	// Components
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(Category = Conductible, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UConductibleComponent* conductibleComponent;
};
