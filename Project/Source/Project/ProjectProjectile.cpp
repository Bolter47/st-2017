// Copyright 1998-2017 Epic Games, Inc. All Rights Reserve

#include "ProjectProjectile.h"
#include "StormTrooper.h"
#include "ShieldComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "ConductibleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/Components/PointLightComponent.h"

///////////
#include "EngineGlobals.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
/////////

AProjectProjectile::AProjectProjectile() 
{
	

	// Get the hit sound
	static ConstructorHelpers::FObjectFinder<USoundBase> HitAudio(TEXT("SoundCue'/Game/SFX/S_Shock.S_Shock'"));
	HitSound = HitAudio.Object;
	// Get travel sound
	static ConstructorHelpers::FObjectFinder<USoundBase> TravelAudio(TEXT("SoundCue'/Game/SFX/S_ProjectileTravel.S_ProjectileTravel'"));
	TravelSoundSource = CreateDefaultSubobject<UAudioComponent>(TEXT("TravelSoundComponent"));
	TravelSoundSource -> SetSound(TravelAudio.Object);

	// Static reference to the mesh to use for the projectile
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ProjectileMeshAsset(TEXT("/Game/TwinStick/Meshes/TwinStickProjectile.TwinStickProjectile"));

	// Create mesh component for the projectile sphere
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh0"));
	ProjectileMesh->SetStaticMesh(ProjectileMeshAsset.Object);
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->BodyInstance.SetCollisionProfileName("Projectile");
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AProjectProjectile::OnHit);		// set up a notification for when this component hits something
	RootComponent = ProjectileMesh;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement0"));
	ProjectileMovement->UpdatedComponent = ProjectileMesh;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = 0.f; // No gravity

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	// ref to on hit particle effect
	static ConstructorHelpers::FObjectFinder<UParticleSystem> HitParticleEffectAsset(TEXT("ParticleSystem'/Game/Particles/P_LightningShockwave_01.P_LightningShockwave_01'"));
	OnHitParticleEffects = HitParticleEffectAsset.Object;
	// ref to the trail effect
	// ref to on hit particle effect
	static ConstructorHelpers::FObjectFinder<UParticleSystem> LightningParticleEffectAsset(TEXT("ParticleSystem'/Game/Particles/P_LightningTrail.P_LightningTrail'"));
	auto particleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TrailParticles"));
	particleComponent->SetupAttachment(RootComponent);
	particleComponent->SetTemplate(LightningParticleEffectAsset.Object);

	// Add light to projectile
	LightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("ProjectileLight"));
	LightComponent->SetupAttachment(RootComponent);
	LightComponent->SetLightColor(FColor(32, 128, 255));
	LightComponent->SetIntensity(10);
	LightComponent->SetLightFalloffExponent(4);
	LightComponent->SetAttenuationRadius(1000);
	LightComponent->bUseInverseSquaredFalloff = false;
}

void AProjectProjectile::Initialize(int charge, AController* shooter)
{
	this->shooter = shooter;
	SetCharges(charge);
}

int AProjectProjectile::GetCharges()
{
	return chargeLeft;
}

void AProjectProjectile::SetCharges(int charges)
{
	if (charges > 0) {
		chargeLeft = charges;
		SetActorScale3D(FVector(chargeLeft, chargeLeft, 1)*ScaleByCharge);
	}
	else {
		Destroy();
	}
}

AController * AProjectProjectile::GetShooter()
{
	return shooter;
}

void AProjectProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Plow through lesser projectiles

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), OnHitParticleEffects, this->GetActorLocation(), this->GetActorRotation(), true);
	UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());

	AProjectProjectile* otherProjectile = Cast<AProjectProjectile>(OtherActor);
	if (otherProjectile && otherProjectile->GetCharges() < GetCharges()) {
		// spawn the projectile
		AProjectProjectile* myBullet = GetWorld()->SpawnActor<AProjectProjectile>(GetActorLocation(), GetActorRotation());
		myBullet->Initialize(chargeLeft, shooter);
	}
		
	Destroy();
}