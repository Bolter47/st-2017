// Fill out your copyright notice in the Description page of Project Settings.

#include "TeamInfo.h"
#include "ColorPalette.h"

void UTeamInfo::AssignPalette(UColorPalette * cp) {
	ColorPalette = cp;
}

void UTeamInfo::SetPrimaryColor(FColor color) {
	// update selected colors list
	ColorPalette->DeselectColor(PrimaryColor);
	PrimaryColor = color;
	ColorPalette->SelectColor(color);
	InvokeOnChange();
}

FColor const UTeamInfo::GetPrimaryColor()
{
	return PrimaryColor;
}

void UTeamInfo::SetSecondaryColor(FColor color) {
	SecondaryColor = color;
	InvokeOnChange();
}

FColor const UTeamInfo::GetSecondaryColor()
{
	return SecondaryColor;
}

void UTeamInfo::SetName(FString name)
{
	Name = name;
}

FString UTeamInfo::GetName()
{
	return Name;
}

void UTeamInfo::InvokeOnChange()
{
	if (OnTeamColorsChanged.IsBound()) {
		OnTeamColorsChanged.Broadcast(PrimaryColor, SecondaryColor);
	}
}

