// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "ConductibleComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FConductorHitDelegate, int, nCharges, AController*, instigator);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECT_API UConductibleComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UConductibleComponent();

protected:
	class NearestConductibleCandidate {
	public: 
		NearestConductibleCandidate(AActor* actor, FVector distance, float angle);
		// Vector pointing from origin actor to this actor
		FVector Distance;
		// The actor we are considering for nearest conductible
		AActor* Actor;
		// The angle between the projectile's speed vector and the direction vector
		float Angle;
	};

	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void OnComponentDestroyed(bool bDestroyHierarchy) override;

	UFUNCTION()
	virtual void ManageCollision(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:	
	FConductorHitDelegate OnProjectileHit;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void AssignCollider(UPrimitiveComponent* collider);
	
	UPROPERTY(EditAnywhere)
	int ChargeReduction = 0;

	UPROPERTY(EditAnywhere)
	float ProjectileFireOffset = 100.0f;

	/**
	* Gets the nearest conductible actor to 'actor' while ignoring 'actor'
	* Returns null if none are found
	*/
	static AActor* GetNearestConductible(AActor* actor);
	/**
	* Gets the nearest conductible actor to 'actor' with the given direction as a tie breaker
	* Returns null if none found
	*/
	static AActor* GetNearestConductible(AActor* actor, FVector direction);

private:

	UPrimitiveComponent* Collider;

	static TArray<AActor*> conductibles;

	static const float NearestConductorThreshold;
};
