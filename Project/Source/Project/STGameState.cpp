// Fill out your copyright notice in the Description page of Project Settings.

#include "STGameState.h"
#include "TeamInfo.h"
#include "PlayerInfo.h"
#include "STGameInstance.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"


UTeamState * ASTGameState::GetPlayerTeam(AController * player)
{
	for (auto team : Teams) {
		auto teamMembers = team.Value->GetMembers();
		for(int j = 0; j < teamMembers.Num(); ++j) {
			if (player == teamMembers[j]) {
				return team.Value;
			}
		}
	}
	return nullptr;
}

void ASTGameState::AssignPlayerToTeam(int32 teamId, AController * player)
{
	if (Teams.Contains(teamId)) {
		Teams[teamId]->AddPlayer(player);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("ADDING PLAYER TO NON EXISTING TEAM"));
	}
}

void ASTGameState::SetupTeamStates()
{
	auto gameInstance = ((USTGameInstance*)GetGameInstance());

	// go through the teams of queued players and add them to their teams
	for (auto queuedPlayer : gameInstance->QueuedPlayers) {
		int teamId = gameInstance->GetPlayerInfo(queuedPlayer)->GetTeamId();
		if (!Teams.Contains(teamId)) {
			// Get the team info
			auto teamInfo = gameInstance->GetTeamInfo(teamId);
			auto state = Teams.Add(teamId, NewObject<UTeamState>());
			state->Init(teamId, teamInfo);
		}
	}
}


void ASTGameState::HandleBeginPlay()
{
	Super::HandleBeginPlay();
	SetupTeamStates();
}

TMap<int32, UTeamState*> & ASTGameState::GetTeams()
{
	return Teams;
}

int ASTGameState::GetTeamScore(int32 teamId)
{
	return Teams[teamId]->GetScore();
}

FColor ASTGameState::GetTeamColor(int32 teamId)
{
	return Teams[teamId]->GetInfo()->GetPrimaryColor();
}

int ASTGameState::GetNumberOfTeam()
{
	return Teams.Num();
}

UTeamState * ASTGameState::GetTeamState(int32 teamId)
{
	if (Teams.Contains(teamId)) {
		return Teams[teamId];
	}
	return nullptr;
}
