// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "STGameInstance.generated.h"


class UPlayerInfo;
class UTeamInfo;
class UColorPalette;
class UGameSettings;

/**
 * 
 */
UCLASS()
class PROJECT_API USTGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	void Init() override;
	void Shutdown() override;

	void InitializePersistentData();
	void ClearPersistentData();

	// Stores Ids of players willing to join
	UPROPERTY()
	TArray<int> QueuedPlayers;

	// Used to add a player to the queued list
	UFUNCTION(BlueprintCallable, Category = "Players")
	void AddPlayerToQueue(int32 playerId);

	UFUNCTION(BlueprintCallable, Category = "Players")
	void RemovePlayerFromQueue(int32 playerId);

	UFUNCTION(BlueprintCallable, Category = "Players")
	int GetNumberOfQueuedPlayers();

	UFUNCTION(BlueprintCallable, Category = "Players")
	void ClearQueuedPlayers();

	UFUNCTION(BlueprintCallable, Category = "Player Info")
	UPlayerInfo* GetPlayerInfo(int id);

	UFUNCTION(BlueprintCallable, Category = "Team Colors")
	UColorPalette* GetColorPalette();

	UFUNCTION(BlueprintCallable, Category = "Team Colors")
	UTeamInfo* GetTeamInfo(int teamId);

	UPROPERTY(BlueprintReadWrite)
	UGameSettings* GameSettings;

	UPROPERTY(BlueprintReadWrite)
	bool InitialStart = true;

private:

	// Stores map names and map paths

	UPROPERTY()
	TArray<UPlayerInfo*> PlayerInfos;
	UPROPERTY()
	TArray<UTeamInfo *> TeamInfos;
	UPROPERTY()
	UColorPalette * ColorPalette;
	UPROPERTY()
	TArray<int> SelectedColors;
};
