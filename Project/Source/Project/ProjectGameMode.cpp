// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "ProjectGameMode.h"
#include "StormTrooper.h"
#include "STPlayerController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "GameFramework/PlayerStart.h"
#include "GameFramework/GameState.h"
#include "GameFramework/PlayerState.h"
#include "EngineUtils.h"
#include "ArenaCamera.h"
#include "STGameState.h"
#include "STGameInstance.h"
#include "TeamInfo.h"
#include "TeamState.h"
#include "PlayerInfo.h"
#include "LevelHUD.h"
#include "UMG.h"
#include "PlayerStats.h"
#include "MeshAtlas.h"
#include "GameSettings.h"

AProjectGameMode::AProjectGameMode()
{
	// Load Atlas
	Atlas = NewObject<UMeshAtlas>();

	// set default classes
	GameStateClass = ASTGameState::StaticClass();
	PlayerControllerClass = ASTPlayerController::StaticClass();
	HUDClass = nullptr;
	DefaultPawnClass = nullptr;
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FClassFinder<UUserWidget> cdWidgetClassFinder(TEXT("/Game/MenuWidgets/CountdownWidget"));
	if (cdWidgetClassFinder.Succeeded()) {
		CountdownWidgetClass = cdWidgetClassFinder.Class;
	}
	static ConstructorHelpers::FClassFinder<UUserWidget> vicWidgetClassFinder(TEXT("/Game/MenuWidgets/VictoryScreenWidget"));
	if (vicWidgetClassFinder.Succeeded()) {
		VictoryWidgetClass = vicWidgetClassFinder.Class;
	}
	static ConstructorHelpers::FObjectFinder<USoundBase> spawnSoundFinder(TEXT("SoundCue'/Game/SFX/S_Blast.S_Blast'"));
	if (spawnSoundFinder.Succeeded()) {
		SpawnSound = spawnSoundFinder.Object;
	}
	static ConstructorHelpers::FObjectFinder<UParticleSystem> spawnEffectFinder(TEXT("ParticleSystem'/Game/Particles/P_SpawnEffect.P_SpawnEffect'"));
	if (spawnEffectFinder.Succeeded()) {
		SpawnEffects = spawnEffectFinder.Object;
	}
}

void AProjectGameMode::BeginPlay() {
	// Load game settings
	GameSettings = ((USTGameInstance*)GetGameInstance())->GameSettings;

	SpawnStartingPlayers();
	StartBeginMatchCountdown();
	SetViewToArenaCamera();
}

void AProjectGameMode::SpawnStartingPlayers()
{
	USTGameInstance* gameInstance = (USTGameInstance *)GetGameInstance();

	auto queuedPlayers = &(gameInstance->QueuedPlayers);
	// If we're in play in editor, we might want some players by default
	if (GetWorld() != nullptr && GetWorld()->IsPlayInEditor() && queuedPlayers->Num() == 0)
	{
		for (int i = 0; i < 4; ++i) {
			queuedPlayers->Add(i);
		}
	}

	// Prepare gamestate to receive players
	auto gameState = GetGameState<ASTGameState>();
	gameState->HandleBeginPlay();

	// Set the default pawn to spawn
	DefaultPawnClass = AStormTrooper::StaticClass();
	HUDClass = ALevelHUD::StaticClass();
	
	// Spawn the queued players
	for (auto playerId : *queuedPlayers) {
		// get the player controller associated to the player
		auto PC = UGameplayStatics::GetPlayerController(GetWorld(), playerId);
		if (PC) {
			InitializeHUDForPlayer(PC);
			// spawn a pawn for this player
			RestartPlayer(PC);
		}
		else {
			// create a player instead
			PC = UGameplayStatics::CreatePlayer(GetWorld(), playerId, true);
			if (!PC) {
				// nothing else we can do
				return;
			}
		}

		// Add the player to its team
		auto playerInfo = gameInstance->GetPlayerInfo(playerId);
		gameState->AssignPlayerToTeam(playerInfo->GetTeamId(), PC);
		auto teamInfo = gameInstance->GetTeamInfo(playerInfo->GetTeamId());

		// Setup player input accordingly
		PC->DisableInput(PC);
		((ASTPlayerController*)PC)->SetControlledByGamepad(playerInfo->ControlType == EControlType::Gamepad);
	}
}

void AProjectGameMode::SetViewToArenaCamera()
{
	auto camera = TActorIterator<AArenaCamera>(GetWorld());
	if (camera) {
		GetWorld()->GetFirstPlayerController()->SetViewTarget(*camera);
	}
}

void AProjectGameMode::StartBeginMatchCountdown()
{

	if (CountdownWidgetClass) {
		CountdownWidget = CreateWidget<UUserWidget>(GetGameInstance(), CountdownWidgetClass);
		CountdownWidget->AddToViewport();
	}

	CountdownEnabled = true;
}

void AProjectGameMode::OnCountdownEnded()
{
	CountdownEnabled = false;
	TimeBeforeStart = 0.0f;
	bMatchInProgress = true;
	TimeLeft = GameSettings->RoundTimeLimit;

	// Give players control over their pawns
	auto PCIter = GetWorld()->GetPlayerControllerIterator();
	for (; PCIter; ++PCIter) {
		PCIter->Get()->EnableInput(PCIter->Get());
	}

	if (CountdownWidget) {
		CountdownWidget->RemoveFromViewport();
	}

	bShouldUseSpawnEffects = true;
}

void AProjectGameMode::Tick(float DeltaTime) {

	// Check the countdown if necessary
	if (CountdownEnabled) {
		if ((TimeBeforeStart -= DeltaTime) <= 0) {
			OnCountdownEnded();
		}
		return;
	}

	// Check if match is over due to time out
	if (bMatchInProgress && (TimeLeft -= DeltaTime) <= 0) {
		OnGameOver();
	}

	// Check if players should be respawned
	for (auto respawnTimer = KilledPlayerRespawnTimers.CreateIterator(); respawnTimer; ++respawnTimer) {
		if ((respawnTimer.Value()+= DeltaTime) >= GameSettings->RespawnTime) {
			RestartPlayer(respawnTimer.Key());

			respawnTimer.RemoveCurrent();
		}
	}

}

AActor * AProjectGameMode::FindPlayerStart_Implementation(AController * NewPlayer, const FString & IncomingName)
{
	TActorIterator<APlayerStart> startPoint(GetWorld());
	AActor* FurthestStart = *startPoint; // Start at first spawnpoint by default
	float furthestDistance = 0.0f;

	for (; startPoint; ++startPoint) {
		auto SpawnPoint = *startPoint;
		float closestPawnDistance = -1.0f;
		for (auto pawn: PlayerPawns) {
			float distance = FVector::Distance(SpawnPoint->GetActorLocation(), pawn->GetActorLocation());
			if (closestPawnDistance < 0 || distance < closestPawnDistance) {
				closestPawnDistance = distance;
			}
		}
		if (closestPawnDistance > furthestDistance) {
			// this spawn point is our new contender
			FurthestStart = SpawnPoint;
			furthestDistance = closestPawnDistance;
		}
	}
	return FurthestStart;
}

APawn * AProjectGameMode::SpawnDefaultPawnFor_Implementation(AController * player, AActor * startSpot)
{


	auto pawn = Super::SpawnDefaultPawnFor_Implementation(player, startSpot);

	if (!pawn) {
		return nullptr;
	}

	if (bShouldUseSpawnEffects) {
		auto spawnTransform = FTransform();
		spawnTransform.SetLocation(startSpot->GetActorLocation());
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SpawnEffects, spawnTransform);
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpawnSound, startSpot->GetActorLocation());
	}


	int32 playerId = UGameplayStatics::GetPlayerControllerID(player->CastToPlayerController());

	auto trooper = ((AStormTrooper*)pawn);
	trooper->OnPlayerKilled.AddDynamic(this, &AProjectGameMode::ManagePlayerDeath);
	
	USTGameInstance* gameInstance = (USTGameInstance *)GetGameInstance();

	auto playerInfo = gameInstance->GetPlayerInfo(playerId);
	auto teamInfo = gameInstance->GetTeamInfo(playerInfo->GetTeamId());

	//Assign model and team colors to material instance
	trooper->SetVisuals(playerInfo->GetHelmetName(), teamInfo->GetPrimaryColor(), teamInfo->GetSecondaryColor());
	trooper->SetLaserVisibility(true);

	PlayerPawns.AddUnique(pawn);

	return pawn;
}

void AProjectGameMode::ManagePlayerDeath(AController * killer, AController* victim)
{
	// Cleanly destroy the killed pawn
	auto victimPawn = victim->GetPawn();
	victim->PawnPendingDestroy(victimPawn);
	victim->UnPossess();
	PlayerPawns.RemoveSingle(victimPawn);
	victimPawn->Destroy();

	// After pawn is destroyed, set the view target
	auto camera = TActorIterator<AArenaCamera>(GetWorld());
	(GetWorld()->GetFirstPlayerController())->SetViewTarget(*camera);

	GameModeSpecificPlayerDeath(killer, victim);
}

void AProjectGameMode::GameModeSpecificPlayerDeath(AController * killer, AController * victim)
{

	// Adjust killer's score

	auto killerTeam = GetGameState<ASTGameState>()->GetPlayerTeam(killer);
	auto victimTeam = GetGameState<ASTGameState>()->GetPlayerTeam(victim);

	// Adjust team stats
	int scoreAdjustment = (killerTeam != victimTeam) ? 1 : -1;
	int newScore = killerTeam->AdjustScore(scoreAdjustment);

	// Adjust player stats
	if (scoreAdjustment > 0) {
		((ASTPlayerController*)killer)->Stats->Kills++;
	}
	else {
		((ASTPlayerController*)killer)->Stats->FriendlyFire++;
	}
	((ASTPlayerController*)victim)->Stats->Deaths++;

	// Check victory
	if (newScore >= GameSettings->ScoreLimit) {
		OnGameOver();
	}
	else {
		// Add killed player to the respawn timers
		KilledPlayerRespawnTimers.Add(victim, 0.0f);
	}
}

void AProjectGameMode::OnGameOver()
{
	if (!bMatchInProgress) {
		return;
	}

	bMatchInProgress = false;

	// Clear pending respawns
	KilledPlayerRespawnTimers.Empty();

	// Check winner and get the best player's pawn if any
	AActor* bestPlayerPawn = nullptr;
	auto teams = GetGameState<ASTGameState>()->GetTeams();
	auto highestScore = 0;
	for (auto teamState : teams) {
		auto score = teamState.Value->GetScore();
		if (score > highestScore) {
			bMatchIsDraw = false;
			highestScore = score;
			WinningTeam = teamState.Value;
			int bestRatio = 0;
			for (auto player : teamState.Value->GetMembers()) {
				auto stats = ((ASTPlayerController*)player)->Stats;
				if ((stats->Kills - stats->FriendlyFire) > bestRatio) {
					bestPlayerPawn = player->GetPawn();
				}
			}
		}
		else if (score == highestScore) {
			bMatchIsDraw = true;
		}
	}


	// Load up victory screen
	if (VictoryWidgetClass) {
		VictoryWidget = CreateWidget<UUserWidget>(GetGameInstance(), VictoryWidgetClass);
		VictoryWidget->AddToViewport();
	}

	// Set the view on the winner
	if (bestPlayerPawn != nullptr) {
		FViewTargetTransitionParams transition;
		transition.BlendFunction = EViewTargetBlendFunction::VTBlend_EaseIn;
		transition.BlendTime = 1.0;
		GetWorld()->GetFirstPlayerController()->SetViewTarget(bestPlayerPawn, transition);
	}
}



void AProjectGameMode::RestartPlayer(AController * player)
{
	Super::RestartPlayer(player);

	// After player is spawned, set the view target
	SetViewToArenaCamera();
}

void AProjectGameMode::HandleMatchHasStarted()
{

	// First fire BeginPlay, if we haven't already in waiting to start match
	GetWorldSettings()->NotifyBeginPlay();

	// Then fire off match started
	GetWorldSettings()->NotifyMatchStarted();

	if (IsHandlingReplays() && GetGameInstance() != nullptr)
	{
		GetGameInstance()->StartRecordingReplay(GetWorld()->GetMapName(), GetWorld()->GetMapName());
	}
}

const float AProjectGameMode::GetTimeBeforeStart()
{
	return TimeBeforeStart;
}

