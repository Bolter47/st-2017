// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ArenaCamera.generated.h"

UCLASS()
class PROJECT_API AArenaCamera : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArenaCamera();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(Category = Laser, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm;

	UPROPERTY(Category = Laser, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;

	FVector CameraOffset = FVector(-400.f, 0.f, 2000.f);
	float CameraRotation = -70.f;
	float SpringArmLength = 2000.f;

	UPROPERTY(EditAnywhere, meta = (ClampMin = "0"))
	int CenterWeight = 2;
	FVector Center;
	
	
};
