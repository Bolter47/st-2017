// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MeshAtlas.generated.h"

class UStaticMesh;
class UMaterialInterface;
/**
 * 
 */
UCLASS(Blueprintable)
class PROJECT_API UMeshAtlas : public UObject
{
	GENERATED_BODY()

public:
	// Sets up the meshes and materials in the atlas
	UMeshAtlas();

	UFUNCTION(BlueprintCallable)
	void GetHelmetMesh(FString helmetName, UStaticMesh* &outMesh, UMaterialInterface* &outMaterial);
	UFUNCTION(BlueprintCallable)
	void GetGunMesh(FString gunName, UStaticMesh* &outMesh, UMaterialInterface* &outMaterial);
	UFUNCTION(BlueprintCallable)
	TArray<FString> GetListOfHelmetNames();
	UFUNCTION(BlueprintCallable)
	TArray<FString> GetListOfGunNames();

private:

	UPROPERTY()
	TMap<FString, UStaticMesh*> HelmetMeshesMap;
	TMap<FString, UMaterialInterface*> HelmetMaterialsMap;
	
	UPROPERTY()
	TMap<FString, UStaticMesh*> GunMeshesMap;
	TMap<FString, UMaterialInterface*> GunMaterialsMap;
	
};
