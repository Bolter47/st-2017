// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "TeamInfo.h"
#include "TeamState.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PROJECT_API UTeamState : public UObject
{
	GENERATED_BODY()
	
public:
	void Init(int number, UTeamInfo* teamInfo);
	// Add a player to this team
	void AddPlayer(AController* player);
	// Adds amount to the team's current score and returns the new score
	int AdjustScore(int amount);
	// returns the team's ID
	int GetNumber();
	// returns the team's score
	UFUNCTION(BlueprintCallable)
	int GetScore();
	// Returns persistent team info
	UFUNCTION(BlueprintCallable)
	UTeamInfo* GetInfo();

	UFUNCTION(BlueprintCallable)
	TArray<AController*> & GetMembers();
private:
	UPROPERTY()
	int Number = 0;
	UPROPERTY()
	int Score = 0;
	UPROPERTY()
	TArray<AController*> Members;
	UPROPERTY()
	UTeamInfo* Info;
	
};
