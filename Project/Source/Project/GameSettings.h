// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameSettings.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PROJECT_API UGameSettings : public UObject
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float RespawnTime = 3.0f;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int ScoreLimit = 10;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool bFriendlyFireEnabled= true;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float RoundTimeLimit = 300.0f;
};
