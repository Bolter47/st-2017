// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaCamera.h"
#include "StormTrooper.h"
#include "EngineUtils.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"


// Sets default values
AArenaCamera::AArenaCamera()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//Players = TArray<AActor*>();

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->SetRelativeLocationAndRotation(CameraOffset, FRotator(CameraRotation, 0.0f, 0.0f));
	SpringArm->TargetArmLength = SpringArmLength;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);

}

// Called when the game starts or when spawned
void AArenaCamera::BeginPlay()
{
	Super::BeginPlay();

    // Save starting pos as level center
	Center = GetActorLocation();
	
}

// Called every frame
void AArenaCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector middlePoint = Center * CenterWeight;
	int nActor = CenterWeight;
	for (TActorIterator<AStormTrooper> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		middlePoint += ActorItr->GetActorLocation();
		++nActor;
	}
	middlePoint /= nActor;

	SetActorLocation(middlePoint);

}

