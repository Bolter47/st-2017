// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "LevelHUD.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_API ALevelHUD : public AHUD
{
	GENERATED_BODY()

	ALevelHUD();
	void BeginPlay();
	

private:
	class UClass* lvlWidgetClass;
	class UUserWidget* lvlWidget;
	
	
};
