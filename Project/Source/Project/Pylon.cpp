// Fill out your copyright notice in the Description page of Project Settings.

#include "Pylon.h"
#include "ConductibleComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
APylon::APylon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/Meshes/LR_PowerCoil_01.LR_PowerCoil_01"));
	// Create the mesh component
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PylonMesh"));
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
	MeshComponent->SetCollisionResponseToChannel(ECC_Vehicle, ECR_Ignore);
	MeshComponent->SetStaticMesh(Mesh.Object);

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> ShipMeshMaterial(TEXT("/Game/Materials/M_Powercoil_02.M_Powercoil_02"));
	MeshComponent->SetMaterial(0, ShipMeshMaterial.Object);

	// Add conductibility
	conductibleComponent = CreateDefaultSubobject<UConductibleComponent>(TEXT("Conductor"));
	conductibleComponent->ProjectileFireOffset = PylonSize;
	conductibleComponent->ChargeReduction = Resistance;
	conductibleComponent->AssignCollider(MeshComponent);
}

// Called when the game starts or when spawned
void APylon::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APylon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

