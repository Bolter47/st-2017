// Fill out your copyright notice in the Description page of Project Settings.

#include "ConductibleComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "ProjectProjectile.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UConductibleComponent::UConductibleComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UConductibleComponent::BeginPlay()
{
	Super::BeginPlay();

	// Assign the current collider or get one from the owner
	AssignCollider(GetOwner()->FindComponentByClass<UPrimitiveComponent>());
	conductibles.AddUnique(GetOwner());
	// ...
}

void UConductibleComponent::OnComponentDestroyed(bool bDestroyHierarchy)
{
	conductibles.RemoveSingle(GetOwner());
	Super::OnComponentDestroyed(bDestroyHierarchy);
}


void UConductibleComponent::ManageCollision(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	AProjectProjectile* projectile = Cast<AProjectProjectile>(OtherActor);
	if (projectile) {

		// save projectile info to pass on to next one
		int charges = projectile->GetCharges();
		AController* shooter = projectile->GetShooter();
		float projectileHeight = projectile->GetActorLocation().Z;
		FVector velocity = projectile->GetVelocity();

		projectile->Destroy();

		if (OnProjectileHit.IsBound()) {
			OnProjectileHit.Broadcast(charges, shooter);
		}

		charges -= ChargeReduction;

		if (charges > 0) {
			// spawn the projectile
			auto hitActor = GetOwner();
			auto nextTarget = GetNearestConductible(hitActor, velocity);
			if (nextTarget) {
				FVector fireVector = nextTarget->GetActorLocation() - hitActor->GetActorLocation();
				fireVector.Z = 0;
				fireVector.Normalize();

				FVector spawnLocation = hitActor->GetActorLocation() + fireVector * ProjectileFireOffset;
				spawnLocation.Z = projectileHeight;
				AProjectProjectile* myBullet = GetWorld()->SpawnActor<AProjectProjectile>(spawnLocation, fireVector.Rotation());
				myBullet->Initialize(charges, shooter);
			}
			
		}
	}
}


// Called every frame
void UConductibleComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UConductibleComponent::AssignCollider(UPrimitiveComponent * collider)
{
	if (!collider) {
		return;
	}

	// clear from previous collider
	if (Collider) {
		Collider->OnComponentHit.RemoveDynamic(this, &UConductibleComponent::ManageCollision);
	}
	// listen to new collider
	
	Collider = collider;
	Collider->OnComponentHit.AddDynamic(this, &UConductibleComponent::ManageCollision);
}

AActor * UConductibleComponent::GetNearestConductible(AActor * actor)
{
	return GetNearestConductible(actor, FVector(0,0,1));
}

AActor * UConductibleComponent::GetNearestConductible(AActor * actor, FVector direction)
{
	auto world = actor->GetWorld();
	FVector origin = actor->GetActorLocation();

	float nearestDistance = -1.0f;
	direction.Normalize();

	// make array of non-hidden conductibles at given distances
	TArray<NearestConductibleCandidate> visibleCandidates;

	for (auto conductible : conductibles) {
		// we ignore the actor from which we perform the calculations
		if (conductible != actor) {
			// raytrace to determine visible conductors
			FVector target = conductible->GetActorLocation();
			FVector distance = target - origin;
			FHitResult traceHit;
			bool hidden = world->LineTraceSingleByChannel(traceHit, origin, target, ECC_Vehicle);

			if (!hidden) {
				float angle = FMath::Acos(FVector::DotProduct(direction, distance.GetSafeNormal()));
				visibleCandidates.Add(NearestConductibleCandidate(conductible, target - origin, angle));
				if ((distance.Size() < nearestDistance) || nearestDistance == -1.0f) {
					nearestDistance = distance.Size();
				}
			}
		}
	}

	// pick best candidate among those within nearest threshold
	AActor* nearestConductible = nullptr;
	float nearestAngle = 180;
	for (auto candidate : visibleCandidates)
	{
		if (
			(FMath::Abs(candidate.Distance.Size() - nearestDistance) < NearestConductorThreshold) &&
			candidate.Angle < nearestAngle
			) {
			nearestConductible = candidate.Actor;
			nearestAngle = candidate.Angle;
		}
	}

	return nearestConductible;
}

TArray<AActor*> UConductibleComponent::conductibles;

const float UConductibleComponent::NearestConductorThreshold = 100.0f;

UConductibleComponent::NearestConductibleCandidate::NearestConductibleCandidate(AActor * actor, FVector distance, float angle)
{
	Actor = actor;
	Distance = distance;
	Angle = angle;
}
