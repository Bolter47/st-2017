// Fill out your copyright notice in the Description page of Project Settings.

#include "MeshAtlas.h"
#include "Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "UObject/ConstructorHelpers.h"

UMeshAtlas::UMeshAtlas() {
	//TODO:: Fill up the atlas with mesh options
	
	/******** HELMETS ********/
	// Helmet 1
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Helmet1Mesh(TEXT("StaticMesh'/Game/Meshes/Shocktrooper-Head_SM.Shocktrooper-Head_SM'"));
	HelmetMeshesMap.Add("MK-117", Helmet1Mesh.Object);
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> Helmet1Mat(TEXT("Material'/Game/Materials/M_ShockTrooper-Head.M_ShockTrooper-Head'"));
	HelmetMaterialsMap.Add("MK-117", Helmet1Mat.Object);
	// Helmet 2
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Helmet2Mesh(TEXT("StaticMesh'/Game/Meshes/SM_ShockTrooper_Head_02.SM_ShockTrooper_Head_02'"));
	HelmetMeshesMap.Add("3-KLOP", Helmet2Mesh.Object);
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> Helmet2Mat(TEXT("Material'/Game/Materials/M_ShockTrooperHead_02.M_ShockTrooperHead_02'"));
	HelmetMaterialsMap.Add("3-KLOP", Helmet2Mat.Object);
}

void UMeshAtlas::GetHelmetMesh(FString helmetName, UStaticMesh *& outMesh, UMaterialInterface *& outMaterial)
{
	outMesh = HelmetMeshesMap[helmetName];
	outMaterial = HelmetMaterialsMap[helmetName];
}

void UMeshAtlas::GetGunMesh(FString gunName, UStaticMesh *& outMesh, UMaterialInterface *& outMaterial)
{
	outMesh = GunMeshesMap[gunName];
	outMaterial = GunMaterialsMap[gunName];
}

TArray<FString> UMeshAtlas::GetListOfHelmetNames()
{
	TArray<FString> output;
	HelmetMeshesMap.GetKeys(output);
	return output;
}

TArray<FString> UMeshAtlas::GetListOfGunNames()
{
	TArray<FString> output;
	GunMeshesMap.GetKeys(output);
	return output;
}
