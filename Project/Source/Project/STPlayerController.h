// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "STPlayerController.generated.h"

/**
 * 
 */
class UPlayerStats;

UCLASS()
class PROJECT_API ASTPlayerController : public APlayerController
{
	GENERATED_BODY()

	ASTPlayerController();
	
public:

	virtual void Tick(float deltaTime);

	UPROPERTY(BlueprintReadonly)
	UPlayerStats* Stats;
	void SetControlledByGamepad(bool gamepadControlled);

protected:
	static const FName MoveForwardBinding;
	static const FName MoveSidewaysBinding;
	static const FName AimForwardBinding;
	static const FName AimSidewaysBinding;

	virtual void SetupInputComponent();


	void FirePressed();
	void FireReleased();

	// void Block();

	void PausePressed();
	
	UPROPERTY(Category = Controls, EditAnywhere)
	bool bControlledByGamePad = true;
	UPROPERTY(Category = Controls, EditAnywhere)
	float AxisDetectionThreshold = 0.2f;

private:
	class UClass* pauseWidgetClass;
	class UUserWidget* pauseWidget;
};
