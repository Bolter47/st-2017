// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ProjectGameMode.generated.h"

class UMeshAtlas;
class UGameSettings;
class UTeamState;

UCLASS(MinimalAPI)
class AProjectGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	AProjectGameMode();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual AActor* FindPlayerStart_Implementation(AController* NewPlayer, const FString& IncomingName) override;
	virtual APawn* SpawnDefaultPawnFor_Implementation(AController* player, AActor* startSpot) override;

	virtual void RestartPlayer(AController* player) override;

	// Overriden to block off restart player at game start
	virtual void HandleMatchHasStarted() override;

	UFUNCTION(BlueprintCallable)
	const float GetTimeBeforeStart();

	UPROPERTY(BlueprintReadWrite)
	UMeshAtlas * Atlas;

	UPROPERTY(BlueprintReadOnly)
	float TimeLeft = 0.0f;

	UPROPERTY(BlueprintReadWrite)
	UTeamState* WinningTeam;

	UPROPERTY(BlueprintReadWrite)
	bool bMatchIsDraw;

	UPROPERTY(BlueprintReadWrite)
	bool bMatchInProgress = false;



protected:
	UFUNCTION()
	// Manages a player's pawn destruction and controller view
	virtual void ManagePlayerDeath(AController* killer, AController* victim);

	// Manages a player death according to the game mode's rules.
	virtual void GameModeSpecificPlayerDeath(AController* killer, AController* victim);

	// Manage when the game is over
	virtual void OnGameOver();

private:

	void StartBeginMatchCountdown();

	void OnCountdownEnded();

	void SpawnStartingPlayers();

	void SetViewToArenaCamera();

	// Countdown variables
	float TimeBeforeStart = 5.0f;
	bool CountdownEnabled = false;
	// Widgets spawned in game mode
	UClass* CountdownWidgetClass;
	UUserWidget* CountdownWidget;
	UClass* VictoryWidgetClass;
	UUserWidget* VictoryWidget;

	UPROPERTY()
	UParticleSystem* SpawnEffects;
	UPROPERTY()
	USoundBase* SpawnSound;

	UPROPERTY()
	UGameSettings* GameSettings;

	UPROPERTY()
	bool bShouldUseSpawnEffects = false;


	TArray<class APawn*> PlayerPawns; //living pawns of players

	
	TMap<AController*, float> KilledPlayerRespawnTimers; // Dead players and their respawn timers
};



