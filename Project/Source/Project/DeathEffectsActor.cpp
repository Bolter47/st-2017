// Fill out your copyright notice in the Description page of Project Settings.

#include "DeathEffectsActor.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"


// Sets default values
ADeathEffectsActor::ADeathEffectsActor()
{
	// Get material and textures for blood decal
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BloodBaseMat1(TEXT("/Game/Materials/M_BloodSplatter_01"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BloodBaseMat2(TEXT("/Game/Materials/M_BloodSplatter_02"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BloodBaseMat3(TEXT("/Game/Materials/M_BloodSplatter_03"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BloodBaseMat4(TEXT("/Game/Materials/M_BloodSplatter_04"));

	TArray<UMaterialInterface*> bloodBaseArray = {
		BloodBaseMat1.Object,
		BloodBaseMat2.Object,
		BloodBaseMat3.Object,
		BloodBaseMat4.Object
	};

	BloodDecalMaterial = UMaterialInstanceDynamic::Create(bloodBaseArray[FMath::RandRange(0, 3)], this, "bloodMat");

	auto BloodDecal = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	BloodDecal->RelativeRotation = FRotator(-90, 0, 0);
	BloodDecal->DecalSize = FVector(512, 512, 512);
	BloodDecal->bDestroyOwnerAfterFade = true;
	BloodDecal->SetDecalMaterial(BloodDecalMaterial);
	RootComponent = BloodDecal;


	// Add a particle component to play a blood splash
	ConstructorHelpers::FObjectFinder<UParticleSystem> BloodParticleName(TEXT("ParticleSystem'/Game/Particles/P_BloodSplash_01.P_BloodSplash_01'"));
	auto ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BloodParticles"));

	ParticleSystem->SetupAttachment(BloodDecal);
	ParticleSystem->RelativeRotation = FRotator(90, 0, 0);
	if (BloodParticleName.Succeeded()) {
		ParticleSystem->Template = BloodParticleName.Object;
	}

	// Get the death sound
	static ConstructorHelpers::FObjectFinder<USoundBase> BloodAudio(TEXT("SoundCue'/Game/SFX/S_BloodSquish.S_BloodSquish'"));
	BloodSoundCue = BloodAudio.Object;

	// Get the death scream
	static ConstructorHelpers::FObjectFinder<USoundBase> ScreamAudio(TEXT("SoundCue'/Game/SFX/S_Scream.S_Scream'"));
	ScreamSoundCue = ScreamAudio.Object;
	

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADeathEffectsActor::BeginPlay()
{
	Super::BeginPlay();

	// Play bloody sound
	UGameplayStatics::PlaySoundAtLocation(this, BloodSoundCue, GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(this, ScreamSoundCue, GetActorLocation());
	
}

// Called every frame
void ADeathEffectsActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

