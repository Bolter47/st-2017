// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "STAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_API USTAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	

public:
	/** Left Lower Leg Offset From Ground, Set in Character.cpp Tick */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	FVector RelativeMoveDirection;
};
