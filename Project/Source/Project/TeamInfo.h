// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TeamInfo.generated.h"

class UColorPalette;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FColorChanged, FColor, newPrimaryColor, FColor, newSecondaryColor);

/**
 * 
 */
UCLASS(Blueprintable)
class PROJECT_API UTeamInfo : public UObject
{
	GENERATED_BODY()
	
public:	
	void UTeamInfo::AssignPalette(UColorPalette * cp);

	UFUNCTION(BlueprintCallable, Category = "Team Colors")
	void SetPrimaryColor(FColor color);
	UFUNCTION(BlueprintCallable, Category = "Team Colors")
	FColor const GetPrimaryColor();
	UFUNCTION(BlueprintCallable, Category = "Team Colors")
	void SetSecondaryColor(FColor color);
	UFUNCTION(BlueprintCallable, Category = "Team Colors")
	FColor const GetSecondaryColor();
	UFUNCTION(BlueprintCallable, Category = "Team Info")
	void SetName(FString name);
	UFUNCTION(BlueprintCallable, Category = "Team Info")
	FString GetName();

	UPROPERTY(BlueprintAssignable, Category = "Team Colors")
	FColorChanged OnTeamColorsChanged;


private:

	void InvokeOnChange();

	UPROPERTY()
	FColor PrimaryColor;
	UPROPERTY()
	FString Name;
	UPROPERTY()
	FColor SecondaryColor;
	UPROPERTY()
	UColorPalette* ColorPalette;

};
