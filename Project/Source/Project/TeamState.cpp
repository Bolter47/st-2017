// Fill out your copyright notice in the Description page of Project Settings.

#include "TeamState.h"

void UTeamState::Init(int number, UTeamInfo* teamInfo) {
	Number = number;
	Info = teamInfo;
}

void UTeamState::AddPlayer(AController * player)
{
	Members.AddUnique(player);
}

int UTeamState::GetNumber()
{
	return Number;
}

TArray<AController*>& UTeamState::GetMembers()
{
	return Members;
}

int UTeamState::GetScore()
{
	return Score;
}

int UTeamState::AdjustScore(int amount)
{
	return Score += amount;
}

UTeamInfo * UTeamState::GetInfo()
{
	return Info;
}

