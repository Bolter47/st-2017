// Fill out your copyright notice in the Description page of Project Settings.

#include "STGameInstance.h"
#include "PlayerInfo.h"
#include "TeamInfo.h"
#include "ColorPalette.h"
#include "GameSettings.h"
#include "MeshAtlas.h"

void USTGameInstance::Init()
{
	InitializePersistentData();
}

void USTGameInstance::Shutdown()
{
	ClearPersistentData();
	QueuedPlayers.Empty();
}

void USTGameInstance::InitializePersistentData()
{
	// Load Color Palette
	ColorPalette = NewObject<UColorPalette>();

	auto colors = ColorPalette->GetColors();

	UMeshAtlas* atlas = NewObject<UMeshAtlas>();

	TArray<FString> teamNames = { "Alpha", "Bravo", "Charlie", "Delta" };

	// Load team info
	for (int32 i = 0; i < 4; ++i) {
		// For now create a default team adding a player to each
		TeamInfos.Add(NewObject<UTeamInfo>());
		TeamInfos[i]->AssignPalette(ColorPalette);
		TeamInfos[i]->SetPrimaryColor(colors[i]);
		TeamInfos[i]->SetSecondaryColor(colors[i]);
		TeamInfos[i]->SetName(teamNames[i]);
	}

	// Load player info
	for (int32 i = 0; i < 4; ++i) {
		// For now create a default player info
		PlayerInfos.Add(NewObject<UPlayerInfo>());
		PlayerInfos[i]->SetTeamId(i);
		PlayerInfos[i]->SetHelmetName(atlas->GetListOfHelmetNames()[0]);
	}

	GameSettings = NewObject<UGameSettings>();
}

void USTGameInstance::ClearPersistentData()
{
	PlayerInfos.Empty();
	TeamInfos.Empty();
	ColorPalette = nullptr;
}

void USTGameInstance::AddPlayerToQueue(int32 playerId)
{
	QueuedPlayers.AddUnique(playerId);
}

void USTGameInstance::RemovePlayerFromQueue(int32 playerId)
{
	QueuedPlayers.RemoveSingle(playerId);
}

int USTGameInstance::GetNumberOfQueuedPlayers()
{
	return QueuedPlayers.Num();
}

void USTGameInstance::ClearQueuedPlayers()
{
	QueuedPlayers.Empty();
}

UPlayerInfo* USTGameInstance::GetPlayerInfo(int id)
{
	return PlayerInfos[id];
}

UColorPalette* USTGameInstance::GetColorPalette()
{
	return ColorPalette;
}

UTeamInfo * USTGameInstance::GetTeamInfo(int teamId)
{
	return TeamInfos[teamId];
}

