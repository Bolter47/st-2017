// Fill out your copyright notice in the Description page of Project Settings.

#include "ColorPalette.h"


UColorPalette::UColorPalette() {
	// build the color palette
	// First half chromatic
	ColorPalette.Add(FColor(254, 39, 18), false); // bright red
	ColorPalette.Add(FColor(128, 64, 0), false); // brown
	ColorPalette.Add(FColor(0, 64, 0), false); // forest green
	ColorPalette.Add(FColor(62, 1, 164), false); // violet
	ColorPalette.Add(FColor(167, 25, 75), false); // ochre
	ColorPalette.Add(FColor(2, 71, 254), false); // blue
										  // Second half chromatic
	ColorPalette.Add(FColor(253, 83, 8), false); // red-orange
	ColorPalette.Add(FColor(249, 188, 2), false); // orange-yellow
	ColorPalette.Add(FColor(102, 176, 51), false); // green
	ColorPalette.Add(FColor(134, 1, 176), false); // purple
	ColorPalette.Add(FColor(255, 0, 255), false); // hot magenta
	ColorPalette.Add(FColor(3, 146, 206), false); // sky blue
										   // Other interesting colors
	ColorPalette.Add(FColor(251, 153, 2), false); //orange
	ColorPalette.Add(FColor(255, 254, 52), false); // yellow
	ColorPalette.Add(FColor(128, 234, 44), false); // yellow-green
	ColorPalette.Add(FColor(128, 128, 255), false); // lavender
	ColorPalette.Add(FColor(250, 129, 122), false); // salmon
	ColorPalette.Add(FColor(128, 255, 255), false); // ice blue
											 // Greys
	ColorPalette.Add(FColor(240, 240, 240), false); // white
	ColorPalette.Add(FColor(178, 178, 178), false); // bright grey
	ColorPalette.Add(FColor(124, 124, 124), false); // mid grey
	ColorPalette.Add(FColor(77, 77, 77), false); // dark grey
	ColorPalette.Add(FColor(44, 44, 44), false); // near black
	ColorPalette.Add(FColor(0, 0, 0), false); // black
}

void UColorPalette::SelectColor(FColor color) {
	if (ColorPalette.Contains(color)) {
		ColorPalette[color] = true;
		InvokeSelectionChange();
	}
}

void UColorPalette::DeselectColor(FColor color)
{
	if (ColorPalette.Contains(color)) {
		ColorPalette[color] = false;
		InvokeSelectionChange();
	}
}

bool UColorPalette::isColorSelected(FColor color)
{
	return ColorPalette[color];
}

TArray<FColor> UColorPalette::GetColors()
{
	TArray<FColor> colors;
	ColorPalette.GenerateKeyArray(colors);
	return colors;
}

void UColorPalette::InvokeSelectionChange()
{
	if (OnColorSelectionChanged.IsBound())
		OnColorSelectionChanged.Broadcast();
}
