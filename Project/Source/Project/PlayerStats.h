// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerStats.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PROJECT_API UPlayerStats : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Kills = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Deaths = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FriendlyFire = 0;
	
};
