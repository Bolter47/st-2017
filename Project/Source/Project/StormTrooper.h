// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "StormTrooper.generated.h"

class UStaticMeshComponent;
class USkeletalMeshComponent;
class UMaterialInstanceDynamic;
class UMeshAtlas;
class USTAnimInstance;
class UAudioComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPawnKilledDelegate, AController*, Killer, AController*, Victim);

UCLASS()
class PROJECT_API AStormTrooper : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AStormTrooper();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void UpdateStun(float DeltaTime);
	void UpdateWeaponCharge(float DeltaTime);

	// Call this to move the pawn
	void Move(FVector MoveDelta, FRotator NewRotation, float DeltaTime);

	// Methods for firing
	void StartCharging();
	void Fire();
	void StopCharging();

	UFUNCTION()
	// Dmg
	void TakeHit(int charges, AController* attacker);

	UFUNCTION(BlueprintCallable)
	void SetVisuals(FString helmetName, FColor primaryColor, FColor secondaryColor);

	UFUNCTION(BlueprintCallable)
		void SetLaserVisibility(bool visible);

	/** Sound to play each time we fire */
	UPROPERTY(Category = Audio, EditAnywhere, BlueprintReadWrite)
	class USoundBase* FireSound;

	FPawnKilledDelegate OnPlayerKilled;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Defaults (Editables in components menu while in editor...)
	int ChargeKillThreshold = 2;
	float ChargeFontSize = 150.f;
	float gunHeight = 100.f;
	FVector2D ChargeBarSize = FVector2D(20.f, 200.f);
	FVector2D StunBarSize = FVector2D(20.f, 200.f);
	
	// Editables
	UPROPERTY(EditAnywhere)
	float MovementSpeed = 1000.0f;
	UPROPERTY(EditAnywhere)
	float RotationSpeed = 800; // In euler angles per second

	UPROPERTY(EditAnywhere)
	float GunDistance = 150.f;
	UPROPERTY(EditAnywhere)
	float ChargeRate = 2.0f;
	UPROPERTY(EditAnywhere)
	float MaxCharge = 4.0f;
	UPROPERTY(EditAnywhere)
	float MaxStunTime = 0.75f;
	
	UPROPERTY(EditAnywhere)
	FVector ChargeBarRelativeLocation = FVector(-80.f, 0.f, 0.f);	
	UPROPERTY(EditAnywhere)
	FVector ChargeCounterRelativeLocation = FVector(-250.f, 0.f, 0.f);
	UPROPERTY(EditAnywhere)
	FVector StunBarRelativeLocation = FVector(80.f, 0.f, 50.f);

	FRotator TargetRotation;

	bool bIsCharging = false; // Is character charging an attack?
	float CurrentCharge = 0.0f; // Accumulated attack charge

	bool bIsStun = false;
	float CurrentStunTime = 0.0f;

	/* PAWN VISUALS COMPONENTS */
	/* The body's mesh component */
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* BodyComponent;
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMaterialInstanceDynamic* BodyMaterial;
	UPROPERTY(Category = Animation, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USTAnimInstance* BodyAnimation;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* HelmetMesh;
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMaterialInstanceDynamic* HelmetMaterial;

	UPROPERTY(Category = Laser, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* HighlightMesh;
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMaterialInstanceDynamic* HighlightMaterial;

	UPROPERTY(Category = Billboard, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMaterialBillboardComponent* ChargeBar;
	UPROPERTY(Category = Billboard, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UMaterialInstanceDynamic* ChargeMaterial;

	UPROPERTY(Category = Billboard, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMaterialBillboardComponent* StunBar;

	UPROPERTY(Category = Text, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTextRenderComponent* ChargeCounter;

	UPROPERTY(Category = Laser, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class ULaserPointerComponent* laserPointer;

	UPROPERTY()
	UAudioComponent* FootstepSoundSource;

	UPROPERTY()
	UAudioComponent* ChargeSoundSource;
};
