// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ColorPalette.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FColorSelectionChanged);

/**
 * 
 */
UCLASS(Blueprintable)
class PROJECT_API UColorPalette : public UObject
{
	GENERATED_BODY()
public:
	UColorPalette();

	UFUNCTION(BlueprintCallable)
	void SelectColor(FColor color);
	UFUNCTION(BlueprintCallable)
	void DeselectColor(FColor color);
	UFUNCTION(BlueprintCallable)
	bool isColorSelected(FColor color);
	UFUNCTION(BlueprintCallable)
	TArray<FColor> GetColors();

	UPROPERTY(BlueprintAssignable, Category = "Team Colors")
	FColorSelectionChanged OnColorSelectionChanged;

private:

	void InvokeSelectionChange();

	UPROPERTY()
	TMap<FColor, bool> ColorPalette;
	
	
	
};
