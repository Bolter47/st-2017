// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerInfo.h"

FString UPlayerInfo::GetHelmetName()
{
	return HelmetName;
}

void UPlayerInfo::SetHelmetName(FString name)
{
	HelmetName = name;
	InvokeOnChange();
}

int UPlayerInfo::GetTeamId()
{
	return TeamIndex;
}

void UPlayerInfo::SetTeamId(int id)
{
	TeamIndex = id;
}

void UPlayerInfo::SetPlayerName(FText name)
{
	if(!name.IsEmpty())
		PlayerName = name;
}

void UPlayerInfo::InvokeOnChange()
{
	if (OnPlayerInfoChanged.IsBound()) {
		OnPlayerInfoChanged.Broadcast(PlayerName, HelmetName);
	}
}

