// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerInfo.generated.h"




UENUM(BlueprintType)
enum class EControlType : uint8 {
	Gamepad,
	Keyboard
};
/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerInfoChanged, FText, playerName, FString, helmetName);

UCLASS(Blueprintable)
class PROJECT_API UPlayerInfo : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable)
	FPlayerInfoChanged OnPlayerInfoChanged;

	UPROPERTY(BlueprintReadOnly)
	FText PlayerName = FText::FromString("John Doe");
	UPROPERTY()
	FName PlayerModel;

	UPROPERTY()
	EControlType ControlType = EControlType::Gamepad;

	UFUNCTION(BlueprintCallable, Category = "Player Info")
	FString GetHelmetName();
	UFUNCTION(BlueprintCallable, Category = "Player Info")
	void SetHelmetName(FString name);

	UFUNCTION(BlueprintCallable, Category = "Player Info")
	int GetTeamId();
	UFUNCTION(BlueprintCallable, Category = "Player Info")
	void SetTeamId(int id);
	UFUNCTION(BlueprintCallable, Category = "Player Info")
	void SetPlayerName(FText name);

private:

	void InvokeOnChange();

	UPROPERTY()
	FString HelmetName = "Hard Helmet";
	UPROPERTY()
	int TeamIndex; // The team this player is a part of
};
