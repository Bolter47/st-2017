// Fill out your copyright notice in the Description page of Project Settings.

#include "STPlayerController.h"
#include "StormTrooper.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/GameMode.h"
#include "EngineUtils.h"
#include "ArenaCamera.h"
#include "PlayerStats.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

const FName ASTPlayerController::MoveForwardBinding("MoveForward");
const FName ASTPlayerController::MoveSidewaysBinding("MoveSideways");
const FName ASTPlayerController::AimForwardBinding("AimForward");
const FName ASTPlayerController::AimSidewaysBinding("AimSideways");


ASTPlayerController::ASTPlayerController() {
	// Set this pawn to be controlled by the lowest-numbered player, change later
	//AutoPossessPlayer = EAutoReceiveInput::Player0;

	PrimaryActorTick.bCanEverTick = true;
	bShowMouseCursor = true;
	Stats = NewObject<UPlayerStats>();

	static ConstructorHelpers::FClassFinder<UUserWidget> pauseWidgetClassFinder(TEXT("/Game/MenuWidgets/PauseMenu"));
	if (pauseWidgetClassFinder.Succeeded())
		pauseWidgetClass = pauseWidgetClassFinder.Class;
}

void ASTPlayerController::Tick(float deltaTime)
{

	if (GetPawn() == nullptr)
		return;

	FVector MovementDirection = FVector(GetInputAxisValue(MoveForwardBinding), GetInputAxisValue(MoveSidewaysBinding), 0);
	MovementDirection = MovementDirection.GetSafeNormal(AxisDetectionThreshold);

	FRotator LookDirection;
	if (bControlledByGamePad) 
	{
		// Calculate aim according to input axis
		FVector LookVector = FVector(GetInputAxisValue(AimForwardBinding), GetInputAxisValue(AimSidewaysBinding), 0);
		LookVector = LookVector.GetSafeNormal(AxisDetectionThreshold);
		LookDirection = LookVector.IsZero() ? GetPawn()->GetRootComponent()->GetComponentRotation() : LookVector.Rotation();
	}
	else 
	{
		// Make user aim at mouse position
		FHitResult Hit(ForceInit);
		bool GotHit = GetHitResultUnderCursor(ECollisionChannel::ECC_WorldStatic, false, Hit);
		if (GotHit) 
		{
			FVector LookVector = Hit.Location - GetPawn()->GetActorLocation();
			LookVector.Z = 0;
			LookDirection = LookVector.Rotation();
		}
		else 
		{
			LookDirection = GetPawn()->GetRootComponent()->GetComponentRotation();
		}
		
	}

	((AStormTrooper*)GetPawn())->Move(MovementDirection, LookDirection, deltaTime);
}

void ASTPlayerController::SetControlledByGamepad(bool gamepadControlled)
{
	bControlledByGamePad = gamepadControlled;
}

void ASTPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Setup attack and block input
	InputComponent->BindAction("Fire", IE_Pressed, this, &ASTPlayerController::FirePressed);
	InputComponent->BindAction("Fire", IE_Released, this, &ASTPlayerController::FireReleased);

	// InputComponent->BindAction("Block", IE_Pressed, this, &ASTPlayerController::Block);

	InputComponent->BindAction("Pause", IE_Pressed, this, &ASTPlayerController::PausePressed);

	// Setup move input
	InputComponent->BindAxis(MoveForwardBinding);
	InputComponent->BindAxis(MoveSidewaysBinding);

	// Setup Aim input
	InputComponent->BindAxis(AimForwardBinding);
	InputComponent->BindAxis(AimSidewaysBinding);
}

void ASTPlayerController::FirePressed()
{
	auto pawn = (AStormTrooper*)GetPawn();
	if (pawn) {
		pawn->StartCharging();
	}
}

void ASTPlayerController::FireReleased() 
{
	auto pawn = (AStormTrooper*)GetPawn();
	if (pawn) {
		pawn->Fire();
	}
}

// DEPRECATED
//void ASTPlayerController::Block()
//{
//	auto pawn = (AStormTrooper*)GetPawn();
//
//	if (pawn) {
//		pawn->Block();
//	}
//}

void ASTPlayerController::PausePressed()
{
	if (pauseWidgetClass)
	{
		auto PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		pauseWidget = CreateWidget<UUserWidget>(PC, pauseWidgetClass);
		SetInputMode(FInputModeUIOnly());
		pauseWidget->AddToViewport();
		Pause();
	}
}